package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func isInCompartment(item int, contents []int) bool {
	result := false
	for _, compareItem := range contents {
		if item == compareItem {
			result = true
			break
		}
	}

	return result
}

func convertCharacterToItem(c rune) int {
	item := 0
	if (c >= rune('a')) && (c <= rune('z')) {
		item = int(c - 'a' + 1)
	} else if (c >= 'A') && (c <= 'Z') {
		item = int(c) - int('A') + 27
	}

	return item
}

func parseRucksack(rucksackText string) []int {
	size := len(rucksackText)
	rucksack := make([]int, size)
	for index, item := range rucksackText {
		rucksack[index] = convertCharacterToItem(item)
	}
	return rucksack
}

func findWrongItems(rucksack []int) int {
	duplicate := 0
	topHalf := rucksack[:len(rucksack)/2]
	bottomHalf := rucksack[len(rucksack)/2:]
	for _, item := range topHalf {
		if isInCompartment(item, bottomHalf) {
			duplicate = item
			break
		}
	}

	return duplicate
}

func checkRucksackForItem(rucksack []int, item int) bool {
	for _, checkItem := range rucksack {
		if item == checkItem {
			return true
		}
	}
	return false
}

func findCommonItem(group [][]int) int {
	for item := 1; item <= 56; item++ {
		foundInAll := true
		for _, rucksack := range group {
			if !checkRucksackForItem(rucksack, item) {
				foundInAll = false
				break
			}
		}
		if foundInAll {
			return item
		}
	}
	return 0
}

func main() {
	wrongScore := 0
	var currentGroup [3][]int
	currentIndex := 0
	badgeScore := 0

	f, err := os.Open("real_data")

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {

		line := scanner.Text()
		rucksack := parseRucksack(line)
		currentGroup[currentIndex] = rucksack
		currentIndex += 1
		if currentIndex == 3 {
			badgeScore += findCommonItem(currentGroup[:])
			currentIndex = 0
		}
		wrongItem := findWrongItems(rucksack)
		wrongScore += wrongItem
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Final wrong score is %d\n", wrongScore)
	fmt.Printf("Final badge score is %d\n", badgeScore)
}
