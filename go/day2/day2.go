package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

type Shape int

const (
	Rock Shape = iota
	Paper
	Scissors
)

type Result int

const (
	Win Result = iota
	Lose
	Draw
)

func getShapeScore(shape Shape) int {
	score := 0
	switch shape {
	case Rock:
		score = 1
	case Paper:
		score = 2
	case Scissors:
		score = 3
	}
	return score
}

func getResultScore(result Result) int {
	score := 0
	switch result {
	case Win:
		score = 6
	case Lose:
		score = 0
	case Draw:
		score = 3
	}

	return score
}

func convertElfPlay(c string) Shape {
	shape := Rock
	switch c {
	case "A":
		shape = Rock
	case "B":
		shape = Paper
	case "C":
		shape = Scissors
	}

	return shape
}

func convertHumanPlay(c string) Shape {
	shape := Rock
	switch c {
	case "X":
		shape = Rock
	case "Y":
		shape = Paper
	case "Z":
		shape = Scissors
	}

	return shape
}

func convertDesiredResult(c string) Result {
	result := Draw
	switch c {
	case "X":
		result = Lose
	case "Y":
		result = Draw
	case "Z":
		result = Win
	}

	return result
}

func getDesiredShape(elfChoice Shape, desiredResult Result) Shape {
	shape := Rock
	switch elfChoice {
	case Rock:
		switch desiredResult {
		case Win:
			shape = Paper
		case Lose:
			shape = Scissors
		case Draw:
			shape = Rock
		}
	case Paper:
		switch desiredResult {
		case Win:
			shape = Scissors
		case Lose:
			shape = Rock
		case Draw:
			shape = Paper
		}
	case Scissors:
		switch desiredResult {
		case Win:
			shape = Rock
		case Lose:
			shape = Paper
		case Draw:
			shape = Scissors
		}
	}

	return shape
}

func getResultForPlayerB(aChoice Shape, bChoice Shape) Result {
	result := Draw

	switch bChoice {
	case Rock:
		switch aChoice {
		case Rock:
			result = Draw
		case Paper:
			result = Lose
		case Scissors:
			result = Win
		}
	case Paper:
		switch aChoice {
		case Rock:
			result = Win
		case Paper:
			result = Draw
		case Scissors:
			result = Lose
		}
	case Scissors:
		switch aChoice {
		case Rock:
			result = Lose
		case Paper:
			result = Win
		case Scissors:
			result = Draw
		}
	}

	return result
}

func getGameScoreForPlayerB(aChoice Shape, bChoice Shape) int {
	return getResultScore(getResultForPlayerB(aChoice, bChoice)) + getShapeScore(bChoice)
}

func main() {
	finalScore := 0

	f, err := os.Open("real_data")

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {

		line := scanner.Text()
		game := strings.Fields(line)
		fmt.Println(game)
		elfPlay := convertElfPlay(game[0])
		shapeToPlay := getDesiredShape(elfPlay, convertDesiredResult(game[1]))
		gameScore := getGameScoreForPlayerB(
			elfPlay,
			shapeToPlay)
		finalScore += gameScore
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Final score is %d\n", finalScore)
}
