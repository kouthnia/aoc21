package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Assignment struct {
	lower int
	upper int
}

type Pair struct {
	elf [2]Assignment
}

func parseAssignment(desc string) Assignment {
	var assignment Assignment
	limits := strings.Split(desc, "-")
	assignment.lower, _ = strconv.Atoi(limits[0])
	assignment.upper, _ = strconv.Atoi(limits[1])
	return assignment
}

func parsePair(line string) Pair {
	var pair Pair
	descriptions := strings.Split(line, ",")
	pair.elf[0] = parseAssignment(descriptions[0])
	pair.elf[1] = parseAssignment(descriptions[1])
	return pair
}

func doesPairOverlap(pair Pair) bool {
	if pair.elf[0].lower <= pair.elf[1].lower && pair.elf[0].upper >= pair.elf[1].upper {
		return true
	}
	if pair.elf[1].lower <= pair.elf[0].lower && pair.elf[1].upper >= pair.elf[0].upper {
		return true
	}
	return false
}

func doesPairOverlapAtAll(pair Pair) bool {
	if doesPairOverlap(pair) {
		return true
	}
	if pair.elf[0].upper >= pair.elf[1].lower && pair.elf[0].upper <= pair.elf[1].upper {
		return true
	}
	if pair.elf[0].lower >= pair.elf[1].lower && pair.elf[0].lower <= pair.elf[1].upper {
		return true
	}
	return false
}

func main() {
	overlaps := 0
	anyOverlaps := 0

	f, err := os.Open("real_data")

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := scanner.Text()
		if doesPairOverlap(parsePair(line)) {
			overlaps++
		}
		if doesPairOverlapAtAll(parsePair(line)) {
			anyOverlaps++
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Complete overlap = %d\n", overlaps)
	fmt.Printf("Any overlap = %d \n", anyOverlaps)
}
