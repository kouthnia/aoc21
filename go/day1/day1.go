package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	current_elf := 1
	current_food := 0
	biggest_elf := 0
	biggest_food := 0
	top_three := [3]int{0}

	f, err := os.Open("real_data")

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {

		line := scanner.Text()
		if line == "" {
			if current_food > top_three[0] {
				top_three[0] = current_food
			}

			if current_food > biggest_food {
				biggest_elf = current_elf
				biggest_food = current_food
			}
			current_food = 0
			current_elf += 1
		} else {
			i, err := strconv.Atoi(line)
			if err != nil {
				log.Fatal(err)
			}
			current_food += i
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	total := 0
	for _, i := range top_three {
		total += i
	}

	fmt.Printf("Elf number %d is the most useful with %d calories\n", biggest_elf, biggest_food)
	fmt.Printf("The top three elves have %d calories\n", total)
}
